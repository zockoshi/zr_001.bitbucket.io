* ORG settings                                                     :noexport:

#+title: Simulations of neutron primary irridiation damage
#+author: Bartosz Barzdajn (The University of Manchester)

** Latex settings
#+Latex_compiler: lualatex # pdflatex 
# #+LATEX_CLASS: beamer
# #+OPTIONS: toc:nil 
# #+LATEX_HEADER: \setbeamercolor{normal text}{fg=white,bg=black}
# #+BEAMER_FRAME_LEVEL: 1


** Other settings

#+PROPERTY: header-args :python :session :eval no-export :exports none :results output replace
#+STARTUP: inlineimages
#+MACRO: color @@html:<font color="$1">$2</font>@@


** reveal.js

#+REVEAL_HLEVEL: 1
#+REVEAL: split
#+REVEAL_INIT_OPTIONS: width:1600, height:1200, controlsLayout: 'edges'

# #+OPTIONS: reveal_center:t reveal_progress:t reveal_history:t reveal_control:t
# #+OPTIONS: reveal_mathjax:t reveal_rolling_links:t reveal_keyboard:t reveal_overview:t num:nil
#+OPTIONS: toc:nil num:nil
# #+OPTIONS: reveal_title_slide:nil
#+OPTIONS: timestamp:nil
# #+REVEAL_MARGIN: 0.1
# #+REVEAL_MIN_SCALE: 0.25
# #+REVEAL_MAX_SCALE: 1.0
#+REVEAL_TRANS: slide
#+REVEAL_THEME: night

#+REVEAL_EXTRA_CSS: ./extra.css
#+REVEAL_ROOT: ./
#+OPTIONS: reveal_single_file:nil


* Problem statement
Small-scale phenomena may result in large scale problems.
#+attr_html: image :class stretch :width 1500
[[./assets/overview.svg]]


* Addressing the problem ...
... from the modelling perspective (an incomplete map).
#+attr_html: image :class stretch :width 1600
[[./assets/multi_scale.svg]]


* Simulating primary radiation damage                              :noexport:

$${\displaystyle i\hbar {\frac {d}{dt}}\vert \Psi ({\mathbf r}, {\mathbf R}, t)\rangle = {\hat {H}}\vert \Psi ({\mathbf r}, {\mathbf R}, t)\rangle}$$
$${\hat {T}}_{n}=-\sum _{i}{\frac {\hbar ^{2}}{2M_{i}}}\nabla _{{{\mathbf {R}}_{i}}}^{2}$$
$${\hat {T}}_{e}=-\sum _{i}{\frac {\hbar ^{2}}{2m_{e}}}\nabla _{{{\mathbf {r}}_{i}}}^{2}$$
$${\hat {U}}_{{en}}=-\sum _{i}\sum _{j}{\frac {Z_{i}e^{2}}{4\pi \epsilon _{0}\left|{\mathbf {R}}_{i}-{\mathbf {r}}_{j}\right|}}$$
$${\hat {U}}_{{ee}}={1 \over 2}\sum _{i}\sum _{{j\neq i}}{\frac {e^{2}}{4\pi \epsilon _{0}\left|{\mathbf {r}}_{i}-{\mathbf {r}}_{j}\right|}}$$
$${\hat {U}}_{{nn}}=\sum _{i}\sum _{{j>i}}{\frac {Z_{i}Z_{j}e^{2}}{4\pi \epsilon _{0}\left|{\mathbf {R}}_{i}-{\mathbf {R}}_{j}\right|}}$$

#+attr_html: image :class stretch :width 1200 
[[./assets/QMD.svg]]


* What happens when a neutron "hits" an atom?                      :noexport:

#+ATTR_REVEAL: :frag (appear)
Quantum molecular dynamics (in one of the many forms)
$$i\hbar\frac{\partial}{\partial t} \psi \left(\vec{r}, t\right) = \hat{H} \left(\psi^\prime \left(\vec{r}, t\right), \vec{R} \right) \psi \left(\vec{r}, t\right)$$
where $\psi$ are single-electron KS orbitals.
#+ATTR_REVEAL: :frag (appear)
*color(orange, How to approach this?*
#+ATTR_REVEAL: :frag (appear)
"For every complex problem there is an answer that is clear, simple, and wrong." \\
H. L. Mencken
#+ATTR_REVEAL: :frag (appear)
*For example embedded-atom method (EAM)*
$${\displaystyle E_{i}=F_{\alpha }\left(\sum _{i\neq j}\rho _{\beta }(r_{ij})\right)+{\frac {1}{2}}\sum _{i\neq j}\phi _{\alpha \beta }(r_{ij})}$$
where $F$ is an embeding potential and $\phi$ is a pair-wise potential.

* What happens when a neutron "hits" an atom?

#+ATTR_REVEAL: :frag (appear)
- High amount of energy is transfered as the kinetic energy of the primary knock-on atom.
- A cascade of collisions is initiated resulting that involves interaction between thousands of atoms and hight temperatures.
- Such scenario is too complicated for /ab initio/ methods 
- E.g. *quantum molecular dynamics* @@html:<br>@@ $$i\hbar\frac{\partial}{\partial t} \psi \left(\vec{r}, t\right) = \hat{H} \left(\psi^\prime \left(\vec{r}, t\right), \vec{R} \right) \psi \left(\vec{r}, t\right)$$
- Fortunately, "for every complex problem there is an answer that is clear, simple, and wrong." (H. L. Mencken)  
- For example, *embedded-atom method (EAM)* @@html:<br>@@ $${\displaystyle E_{i}=F_{\alpha }\left(\sum _{i\neq j}\rho _{\beta }(r_{ij})\right)+{\frac {1}{2}}\sum _{i\neq j}\phi _{\alpha \beta }(r_{ij})},$$ @@html:<br>@@ where $F$ is an embeding potential and $\phi$ is a pair-wise potential.

* What can we do with it?
Simulate couple of millions of atoms of course. 
@@html:<small>Example sim. showing high-energy atoms in hcp Zr after 10 keV collision.</small>@@
#+attr_html: image :class stretch :width 1600
[[./assets/cascade_frame.svg]]


* Why do we need need more MD simulations of irridiation damage?

#+ATTR_REVEAL: :frag (appear)
There are several published collision cascades for $\alpha - \mathrm{Zr}$. However,
#+ATTR_REVEAL: :frag (appear)
- Sample randomisation is sub-optimal (momenta directions, PKA en., etc.). @@html:<br><small>Ususally is one or the other.</small>@@
- Data is provided as ambiguous summary statistics.
- There is lack of simulations that take electron-phonon coupling into account (for this particular phase).
- Older publications are using small computational cells. @@html:<br><small>I don't know how they were able to get away with it.</small>@@
- Detailed analysis will help us to better understanding displacement cascades. @@html:<br><small>Access to complete information.</small>@@
- Simulations that use current potentials will help us to develop new and improved models (GAP) -- *multi-fidelity models*.


* Since we have to be carefull with that ...
... as there is usually more than one wrong answer.
#+attr_html: image :class stretch :width 1200
[[./assets/zbl_vs_eam_and_ec.svg]]


* Electron-phonon coupling
We also use two-temperature model (first time in case of a pure Zr, ... I think).
#+attr_html: image :class stretch :width 1400
[[./assets/pka_ttm_temp.svg]]
#+attr_html: image :class stretch :width 720
[[./assets/ttm-diag.svg]]
#+ATTR_REVEAL: :frag (appear)
@@html:<small>BTW, this one can make a big difference in terms of number of induced defects.</small>@@


* Primary radiation damage MD simulation
@@html:<img src="./assets/pka_zr_10keV_02s.gif" alt="PKA" style="width:1800px">@@


* Analysis of "global" properties 

#+attr_html: image :class stretch :width 1400
[[./assets/pka_meta.svg]]


* Defect analysis
@@html:<img src="./assets/pka_clusters_10keV.gif" alt="PKA" style="width:1280px;">@@
#+attr_html: image :class stretch :width 1200
[[./assets/sim_cov_vol_groups.svg]]


* Analysis in the context of multiple simulations
Some collisions produce small clusters of defects.
#+attr_html: image :class stretch :width 1400
[[./assets/def_len_rad.svg]]


* Patterns in defects evolution
We can use this to speed-up generation of data and to better understand the process.
#+attr_html: image :class stretch :width 1600
[[./assets/pka_num_ev.svg]]


* Statistical description of the population evolution

#+attr_html: image :class stretch :width 1600
[[./assets/gmm_toy.svg]]


* Not so fancy slide with some fancy buzzwords
- Bayesian latent variable model
- Multi-fidelity modeling and simulation
- Gaussian process regression with kernel methods
... and anything else we can find in PyMC3, Scikit-learn or in SheffieldML Github repository.

put aa fancy image from these pdfs


* Thank you for listening.
Any questions?
